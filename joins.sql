-- cross join, without any join condition
select * from employee emp, mobile mob;

-- inner join
select * from employee emp, mobile mob where emp.id=mob.emp_id;

select * from employee emp inner join mobile mob on emp.id=mob.emp_id;

-- outer join
-- left outer join
select * from employee emp left outer join mobile mob on emp.id=mob.emp_id;

-- right outer join
select * from employee emp right outer join mobile mob on emp.id=mob.emp_id;

-- full outer join
select * from employee emp left outer join mobile mob on emp.id=mob.emp_id
union
select * from employee emp right outer join mobile mob on emp.id=mob.emp_id;

-- self join

