-- Insert Trigger
DELIMITER $$
create trigger ins_trigger after insert on first_table FOR EACH ROW 
begin
	insert into first_table_audit (first_name, emp_age, email, emp_height, last_name, operation) 
    values (NEW.first_name, NEW.emp_age, NEW.email, NEW.emp_height, NEW.last_name, 'insert');
end$$
DELIMITER ;

-- Update Trigger
DELIMITER $$
create trigger upd_trigger after update on first_table FOR EACH ROW 
begin
	insert into first_table_audit (first_name, emp_age, email, emp_height, last_name, operation) 
    values (old.first_name, old.emp_age, old.email, old.emp_height, old.last_name, 'update');
end$$
DELIMITER ;

-- Delete Trigger
DELIMITER $$
create trigger del_trigger after delete on first_table FOR EACH ROW 
begin
	insert into first_table_audit (first_name, emp_age, email, emp_height, last_name, operation) 
    values (old.first_name, old.emp_age, old.email, old.emp_height, old.last_name, 'delete');
end$$
DELIMITER ;
