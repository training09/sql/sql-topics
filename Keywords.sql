-- distinct (this will always returns unique records, removes duplicates)
-- alias (this will give user, the provision to change the column name in the resultset without changing the source table)
select distinct concat(last_name, " ", first_name) as FULL_NAME  from first_table;

-- where clause (filter the rows)
-- and operator (check both conditons for true, else won't return anything) 
select * from first_table WHERE emp_age >= 25 and emp_height < 150;

-- or operator (check any one conditon is true, else won't return anything)
select * from first_table WHERE emp_age >= 25 or emp_height < 150;

-- IN operator
-- solution without in operator
select * from first_table WHERE first_name = 'giri' or first_name = 'guna' or first_name = 'pasu' or first_name = 'theeran';
-- solution with in operator
select * from first_table WHERE first_name in ('giri','guna','pasu','theeran');

-- Between Operator
-- solution without between operator
select * from first_table where emp_age <=25 and emp_age >=23;
-- solution with between operator
select * from first_table where emp_age  between 23 and 25;

-- like operator * wildcards (%,_)
select * from first_table where first_name like 'ka%';

select * from first_table where first_name like '_a%';

-- union (merge two result set, but don't allow duplicates)
select first_name, email, last_name from first_table where emp_age <=23
union
select first_name, email, last_name from first_table where emp_age >26;

-- union all (merge two result set, but allow duplicates)
select first_name, email, last_name from first_table where emp_age <=23
union all
select first_name, email, last_name from first_table where emp_age >26;

-- limit (it limits the number of rows in the resultset)
SELECT * FROM first_table limit 10;

-- case (this can be used in selector, we can write our condition)
SELECT first_name, Last_name, emp_age,
    CASE
        WHEN emp_age >= 18 THEN 'Major'
        ELSE 'Minor'
    END AS AgeType
FROM first_table;

-- order by (sorts the emp_age, then first_name, then last_name)
-- sorting first level, second level, third level
-- sort asc - ascending (default), desc - descending
SELECT  emp_age, first_name, Last_name, 
    CASE
        WHEN emp_age >= 18 THEN 'Major'
        ELSE 'Minor'
    END AS AgeType
FROM first_table order by emp_age, first_name desc;

-- Query inside the Query
select count(*) from (
select distinct concat(last_name, " ", first_name) as FULL_NAME  from first_table) as t1;

-- describes the TABLE
describe first_table;
desc first_table;

-- drop the functions if already exists
drop function if exists firstFunction;
drop function if exists addValues;
