-- drop the functions if already exists
drop function if exists firstFunction;
drop function if exists addValues;

-- syntax to create the function 
create function firstFunction (input varchar(10))
returns varchar(50) deterministic
return concat('Hello ', input, ', Welcome to the SQL Learning');

create function addValues (input1 int, input2 int)
returns int deterministic
return input1 + input2;

-- run the function
select firstFunction("Tiger") as Output;
